# +12V driver board

Control up to 8 +12V pumps/valves over i2c.

# Firmware

Flash firmware with:

```
avrdude -P <com port> -b 19200 -c avrisp -p m328p -v -U lfuse:w:0xE2:m -U hfuse:w:0xD4:m -U efuse:w:0xFD:m
avrdude -P <com port> -b 19200 -c avrisp -p m328p -v -e -V -U flash:w:hv-switching-board-firmware-v0.10-twiboot.hex:i
```

* HV switching board firmware v0.10
* twiboot I2C bootloader (allowing future firmware updates over I2C)
* Fuses to: internal 8MHz clock, preserve EEPROM across chip erases, and bootloader address to match twiboot location